﻿using AutoMapper;
using Avalia.SmartLab.Filters;
using Avalia.SmartLab.Models;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Avalia.SmartLab.Controllers
{
    [IdentityBasicAuthentication]
    public class UserController : ApiController
    {
        public MongoRepository<User> Users
        {
            get { return _users ?? (_users = new MongoRepository<User>()); }
            set { _users = value; }
        }
        MongoRepository<User> _users;

        [Route("user/{app_user_id}")]
        public IHttpActionResult Get(long app_user_id)
        {
            return GetUser(app_user_id);
        }

        [Route("user")]
        public async Task<IHttpActionResult> Post()
        {
            var user = await Request.Content.ReadAsAsync<UserDTO>();
            return SaveUser(UserMapping.ToEntity(user));
        }

        [Route("user/{app_user_id}")]
        public async Task<IHttpActionResult> Patch()
        {
            var user = await Request.Content.ReadAsAsync<UserDTO>();
            return SaveUser(UserMapping.ToEntity(user));
        }

        private IHttpActionResult GetUser(long app_user_id)
        {
            var user = Users.FirstOrDefault(x => x.Id == app_user_id.ToString());
            if (user == null)
            {
                return NotFound();
            }
            return Ok(UserMapping.ToDTO(user));
        }

        private IHttpActionResult SaveUser(User user)
        {
            try
            {
                Users.Collection.Save(user);
                return Ok(UserMapping.ToDTO(user));
            }
            catch (Exception ex)
            {
                var model = new ResultModel();
                model.code = 1;
                model.message = ex.GetBaseException().Message;
                return Ok(model);
            }
        }
    }
}
