﻿using Avalia.SmartLab.Models;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Avalia.SmartLab.Controllers
{
    [Authorize]
    public class AppController : Controller
    {
        public MongoRepository<User> Users
        {
            get { return _users ?? (_users = new MongoRepository<User>()); }
            set { _users = value; }
        }
        MongoRepository<User> _users;

        public ActionResult Index()
        {
            var user_id = User.Identity.Name;
            var user = Users.FirstOrDefault(x => x.Id == user_id);
            var model = UserMapping.ToDTO(user);
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("Home.Index");
        }
    }
}