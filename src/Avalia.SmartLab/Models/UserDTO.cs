﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avalia.SmartLab.Models
{
    public class UserDTO
    {
        public object user_id { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string surname { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public DateTime birthdate { get; set; }
        public string gender { get; set; }
        public string role { get; set; }
        public Student student { get; set; }
    }

    public class StudentDTO
    {
        public SchoolDTO school { get; set; }
        public SchoolClassDTO school_class { get; set; }
    }

    public class SchoolDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string federative_unit { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
    }

    public class SchoolClassDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string stage { get; set; }
    }
}