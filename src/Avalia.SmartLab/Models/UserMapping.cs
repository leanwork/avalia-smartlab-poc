﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avalia.SmartLab.Models
{
    public static class UserMapping
    {
        public static UserDTO ToDTO(User user)
        {
            Mapper.CreateMap<User, UserDTO>()
                .ForMember(dest => dest.user_id,
                           opts => opts.MapFrom(src => src.Id));

            return Mapper.Map<UserDTO>(user);
        }

        public static User ToEntity(UserDTO user)
        {
            Mapper.CreateMap<UserDTO, User>();
                //.ForMember(dest => dest.Id,
                //           opts => opts.MapFrom(src => src.user_id));

            return Mapper.Map<User>(user);
        }
    }
}