﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avalia.SmartLab.Models
{
    public class ResultModel
    {
        public int code { get; set; }
        public string message { get; set; }
        public object fields { get; set; }
    }
}