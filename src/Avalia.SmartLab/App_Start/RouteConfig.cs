﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Avalia.SmartLab
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Home.Index",
                url: "",
                defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
                name: "App.Index",
                url: "app",
                defaults: new { controller = "App", action = "Index" }
            );
            routes.MapRoute(
                name: "App.Logout",
                url: "app/sair",
                defaults: new { controller = "App", action = "Logout" }
            );
            routes.MapRoute(
                name: "Launch.Index",
                url: "launch",
                defaults: new { controller = "Launch", action = "Index" }
            );
        }
    }
}
